import React from 'react'
import { Icons } from 'constants/constants'
import { InputGroup,FormControl } from 'react-bootstrap'
import { ImageIconsStyle , InputSearchStyle , ComponentSearch} from './styles/search.style'
export default props =>

<ComponentSearch>
  <ImageIconsStyle src={Icons.icSearch} />
  <InputSearchStyle 
    type="text" 
    value={props.value}  
    onChange={props.onChange}
    placeholder={ props.placeholder == null ? 'Tìm Kiếm... ' : props.placeholder}
    />
</ComponentSearch>