import styled from 'styled-components'
import { Colors } from 'constants/constants'

export const ImageIconsStyle = styled.img`
  height: 13px;
  margin-right: 10px;
`
export const InputSearchStyle = styled.input`
  border: 0px;
  width: 90%;
  height: 35px;
  background-color: ${Colors.grayStt};
  padding: 10px 5px 10px 0;
  left: 10px;
  outline: none;

  ::placeholder {
    font-style: italic;
    color: ${Colors.blackTxt};
  }
`
export const ComponentSearch = styled.div`
  padding: 5px 10px;
  background-color: ${Colors.grayStt};
  width: 250px;
  border-radius: 8px;
  margin-left: 23px;
`
