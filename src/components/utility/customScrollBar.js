import React from "react";
import Scrollbar from "react-smooth-scrollbar";

export default ({ id, children, className }) => (
  <Scrollbar id={id} className={className} continuousScrolling={true}>
    {children}
  </Scrollbar>
);
