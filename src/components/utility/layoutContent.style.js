import styled from 'styled-components'
import { palette } from 'styled-theme'

const LayoutContentStyle = styled.div`
  border: 1px solid ${palette('border', 0)};
  height: 100%;
`

export default LayoutContentStyle
