import styled from 'styled-components'

const LayoutContentWrapper = styled.div`
  clear: both;
  padding: 25px 0 0 0;
  @media only screen and (max-width: 500px) {
    padding: 25px 12px 25px 12px;
  }
`

export { LayoutContentWrapper }
