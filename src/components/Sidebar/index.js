import React from 'react'
import $ from 'jquery'
import { Link } from 'react-router-dom'
import { Layout } from 'antd'
import { connect } from 'react-redux'
import { SidebarWrapper, OutButtonStyle } from './styles'

import Menu from 'components/uielements/menu'
import Scrollbars from 'components/utility/customScrollBar.js'
import { getWidth, Colors, Icons } from 'constants/constants'
import redirect from 'containers/redirect'

const { Sider } = Layout

class Sidebar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      collapsed: true
    }
  }

  static defaultProps = {
    options: [
      {
        key: 'a',
        label: 'A',
        icon: 'a',
        subItems: [
          {
            title: 'a',
            key: 'aa'
          }
        ]
      },
      {
        key: 'b',
        icon: 'b',
        label: 'B',
        subItems: [
          {
            title: 'b',
            key: 'bb'
          }
        ]
      }
    ]
  }

  isNotEmpty = obj => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return true
    }
    return false
  }

  getMenuItem = ({ singleOption, submenuColor }) => {
    const { key, icon, label, subItems } = singleOption
    return this.isNotEmpty(subItems) ? (
      <Menu.SubMenu
        key={key}
        title={
          <div>
            <img
              src={icon}
              style={{ width: '15px', height: '15px', marginRight: '10px' }}
            />

            <span style={submenuColor} className="nav-text">
              {' '}
              {label}{' '}
            </span>
          </div>
        }
      >
        {subItems.map(subItem => (
          <Menu.Item key={subItem.key}>
            <Link to={`/${subItem.key}`}>
              <div>
                <span style={submenuColor}>{subItem.title}</span>
              </div>
            </Link>
          </Menu.Item>
        ))}
      </Menu.SubMenu>
    ) : key === '' ? (
      <Menu.Divider />
    ) : (
      <Menu.Item key={key}>
        <Link to={`/${key}`}>
          <span className="isoMenuHolder" style={submenuColor}>
            <img
              src={icon}
              style={{ width: '15px', height: '15px', marginRight: '10px' }}
            />
            <span className="nav-text">{label}</span>
          </span>
        </Link>
      </Menu.Item>
    )
  }

  logout = () => {
    localStorage.clear()
    this.props.logout()
  }

  render() {
    let currentPath = window.location.pathname.slice(
      1,
      window.location.pathname.length
    )
    const { options, collapsed } = this.props

    const customizedTheme = {
      textColor: '#000',
      backgroundColor: '#fff'
    }

    const styling = {
      backgroundColor: Colors.whiteTxt
    }

    const submenuStyle = {
      backgroundColor: '#44f3',
      color: customizedTheme.textColor
    }

    const submenuColor = {
      color: customizedTheme.textColor
    }

    return (
      <SidebarWrapper>
        <Sider
          className="isomorphicSidebar"
          width="280"
          style={styling}
          breakpoint="md"
          collapsedWidth="0"
        >
          <Scrollbars>
            <Menu
              defaultSelectedKeys={[currentPath]}
              className="isoDashboardMenu"
              mode="inline"
              theme="dark"
              inlineCollapsed={this.state.collapsed}
            >
              {options.map(singleOption =>
                this.getMenuItem({ submenuStyle, submenuColor, singleOption })
              )}
            </Menu>
            <OutButtonStyle href="/" onClick={this.logout}>
              <div
                className="row"
                style={{ paddingLeft: '38px', width: '300px' }}
              >
                <img src={Icons.icDangXuat} height="25px" width="25px" />
                <p style={{ marginLeft: '10px', marginTop: '1px' }}>
                  Đăng Xuất
                </p>
              </div>
            </OutButtonStyle>
          </Scrollbars>
        </Sider>
      </SidebarWrapper>
    )
  }
}
const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(actions.logout())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar)
