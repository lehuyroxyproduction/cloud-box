import styled from 'styled-components'
import { palette } from 'styled-theme'
import { transition, borderRadius } from 'settings/style-util'
import WithDirection from 'settings/withDirection'
import { Colors, Fonts } from '../../../constants/constants'

const SidebarWrapper = styled.div`
  padding: 70px 0 0 0;
  background-color: rgb(72, 77, 95);
  min-height: 100vh;
  .isomorphicSidebar {
    height: 100%;
    z-index: 1;
    padding-top: 57px;
    background: ${palette('secondary', 0)};
    background-color: #ffffff;
    flex: 0 0 320px;

    .scrollarea {
      height: calc(100vh - 70px);
    }

    .isoLogoWrapper {
      background: transparent;
      margin: 0;
      padding: 46px 10px;
      text-align: center;
      overflow: hidden;
      ${borderRadius()};

      h3 {
        a {
          font-family:${Fonts.Normal}
          font-size: 21px;
          font-weight: 300;
          letter-spacing: 3px;
          text-transform: uppercase;
          color: ${palette('grayscale', 6)};
          display: block;
          text-decoration: none;
        }
      }
    }

    .ant-layout-sider-zero-width-trigger {
      background:transparent;
      top: 0px;
      color: #484d5f;
    }

    .anticon:before {
      font-size: 25px;
    }

    &.ant-layout-sider-collapsed {
      .isoLogoWrapper {
        padding: 20px 0;

        h3 {
          a {
            font-size: 27px;
            font-weight: 500;
            letter-spacing: 0;
          }
        }
      }
    }

    .isoDashboardMenu {
      background: transparent;
      color: ${Colors.blacktxt} !important;

      a {
        text-decoration: none;
        font-weight: 400;
      }

      .ant-menu-item {
        width: 100%;
        display: -ms-flexbox;
        display: flex;
        flex-direction: column;
        margin: 0;
        padding: 0 !important;
        position: relative;
        height: auto !important;
        > a {
          padding: 0 24px;
        }
      }

      .isoMenuHolder {
        display: flex;
        align-items: center;

        img {
          height: 25px !important;
          width: 25px !important;
        }
        i {
          font-size: 19px;
          color: inherit;
          margin: ${props =>
            props['data-rtl'] === 'rtl' ? '0 0 0 30px' : '0 30px 0 0'};
          width: 18px;
          ${transition()};
        }
      }

      .anticon {
        font-size: 18px;
        margin-right: 30px;
        color: inherit;
        ${transition()};
      }

      .nav-text {
        font-size: 14px;
        color: inherit;
        font-weight: 400;
        ${transition()};
      }

      .ant-menu-item-selected {
        background-color: ${Colors.grayBg} !important;

        .anticon {
          color: #fff;
        }

        i {
          color: ${Colors.blackTxt};
        }

        .nav-text {
          color: ${Colors.blackTxt};
          font-weight: "bold";
        }
        > a {
          border-left: 3px solid ${Colors.blueMain};
          padding: 0 24px 0 21px;
          color: #000000;
        }
        > span {
          color: ${Colors.blackTxt}
        }
      }

      > li {
        &:hover {
          i,
          .nav-text {
            color: ${Colors.blueMain};
          }
        }
      }
    }
    
    .ant-menu-submenu-inline,
    .ant-menu-submenu-vertical {
      > .ant-menu-submenu-title {
        width: 100%;  
        display: flex;
        align-items: center;
        padding: 0 24px;
        color: #000;
        img {
          height: 25px !important;
          width: 25px !important;
          margin-right: 6px !important;
        }
        > span {
          display: flex;
          align-items: center;
          font-weight:"bold";
        }

        .ant-menu-submenu-arrow {
          left: ${props => (props['data-rtl'] === 'rtl' ? '25px' : 'auto')};
          right: ${props => (props['data-rtl'] === 'rtl' ? 'auto' : '25px')};

          &:before,
          &:after {
            background-color:#000;
            ${transition()};
          }

        }

        &:hover {
          .ant-menu-submenu-arrow {
            &:before,
            &:after {
              color: #ffffff;
            }
          }
        }
      }

      .ant-menu-inline,
      .ant-menu-submenu-vertical {
        > li:not(.ant-menu-item-group) {
          font-size: 13px;
          font-weight: 400;
          margin: 0;
          color: ${Colors.blackTxt};
          ${transition()};
          a {
            color: ${Colors.blackTxt};
            font-weight:"bold";
          }
          &:hover {
            a {
              color: #067AF7 !important;
            }
          }
        }

        .ant-menu-item-group {
          padding-left: 0;

          .ant-menu-item-group-title {
            padding-left: 100px !important;
          }
          .ant-menu-item-group-list {
            .ant-menu-item {
              padding-left: 125px !important;
            }
          }
        }
      }

      .ant-menu-sub {
        box-shadow: none;
        background-color: transparent !important;
        .ant-menu-item {
          >a {
            color: #a59d9f ;
          }
          div > span {
            margin-left: 40px;
          }
        }
      }
    }

      .ant-menu-submenu-inline > {
        .ant-menu-submenu-title:after {
          display: none;
        }
      }

      .ant-menu-submenu-vertical {
        > .ant-menu-submenu-title:after {
          display: none;
        }

        .ant-menu-sub {
          background-color: transparent !important;

          .ant-menu-item {
            height: 35px;
          }
        }
      }
    }
  }
`

export default WithDirection(SidebarWrapper)
