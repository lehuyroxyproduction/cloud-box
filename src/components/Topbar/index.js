import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { Layout } from 'antd'

import { TopbarWrapper } from './styles'

import TopbarUser from './topbarUser'
import { actions, selectors } from 'redux/modules/auth-reducer'
import { Colors, Fonts } from 'constants/constants'
import logo from 'public/images/logo.png'

const { Header } = Layout

class Topbar extends React.Component {
  state = { collapsed: true }

  static defaultProps = {
    name: null,
    role: 'jobname',
    avatar: null
  }

  toggleCollapsed = () => this.setState({ collapsed: !this.state.collapsed })

  logout = () => {
    localStorage.clear()
    this.props.logout()
  }

  render() {
    const { avatar, collapsed } = this.props
    let user = { name: '', role: '' }
    user = JSON.parse(localStorage.getItem('user'))

    const customizedTheme = {
      textColor: 'black',
      backgroundColor: Colors.whiteTxt
    }

    const styling = {
      background: customizedTheme.backgroundColor,
      position: 'fixed',
      width: '100%',
      height: 70
    }
    const styleLogo = {
      color: Colors.blueMain,
      fontWeight: 'lighter',
      fontFamily: Fonts.boldItalic
    }
    return (
      <TopbarWrapper>
        <Header
          style={styling}
          className={
            collapsed ? 'isomorphicTopbar collapsed' : 'isomorphicTopbar'
          }
        >
          <div className="isoLeft">
            {/*<img src={} alt="logo" >*/}
            <Link to="/thong-tin">
              <img src={logo} width="130px" height="50px" />
            </Link>
          </div>

          <TopbarUser
            name={user ? user.name : ''}
            role={user ? user.role : ''}
            avatar={avatar}
            logout={this.logout}
          />
        </Header>
      </TopbarWrapper>
    )
  }
}

const mapStateToProps = state => ({
  user: selectors.getUser(state)
})
const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(actions.logout())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Topbar)
