import styled from 'styled-components'
import { palette } from 'styled-theme'
import { Colors } from 'constants/constants'

const AppHolder = styled.div`
  .trigger {
    font-size: 18px;
    line-height: 64px;
    padding: 0 16px;
    cursor: pointer;
    transition: color 0.3s;
  }

  .trigger:hover {
    color: ${palette('primary', 0)};
  }

  .ant-layout-sider-collapsed .anticon {
    font-size: 16px;
  }

  .ant-layout-sider-collapsed .nav-text {
    display: none;
  }

  .ant-layout {
    background: ${palette('secondary', 1)};

    &.isoContentMainLayout {
      overflow: auto;
      overflow-x: hidden;
      padding: 82px 14px 27px;

      @media only screen and (max-width: 500px) {
        padding: 100px 12px 27px;
      }
    }
  }

  .bodyContent {
    padding-bottom: 20px;
    background-color: ${Colors.whiteTxt};
    border-radius: 8px;
    box-shadow: 0 2px 10px 0 rgba(201, 201, 201, 0.5);
  }

  .isoLayoutContent {
    width: 100%;
    padding: 25px 0;
    background-color: #ffffff;
    height: 100%;
  }

  .isomorphicLayout {
    width: calc(100% - 240px);
    flex-shrink: 0;
    overflow-x: hidden !important;

    @media only screen and (max-width: 767px) {
      width: 100%;
    }

    @media only screen and (min-width: 768px) and (max-width: 1220px) {
      width: calc(100% - 80px);
      width: 100%;
    }
  }

  .ant-layout-footer {
    font-size: 13px;
    @media (max-width: 767px) {
      padding: 10px 20px;
    }
  }

  button {
    border-radius: 0;
  }
`

export default AppHolder
