import React from 'react'
import Proptypes from 'prop-types'
import { Layout } from 'antd'

// custom components
import { Sidebar, Topbar } from 'components'
import PageHeader from 'components/utility/pageHeader'
import ComponentBreadCrumbWrapper from 'components/utility/breadCrumbs'
import LayoutContentWrapper from 'components/utility/layoutWrapper.js'

// wrapper
import { AppHolder, DashAppHolder } from './styles'

const { Content } = Layout

export default class Dashboard extends React.Component {
  state = { collapsed: false, openDrawer: true }

  static propTypes = {
    routes: Proptypes.array
  }
  componentWillMount() {
    console.log(window.innerHeight)
  }
  onDrawerClick = () =>
    this.setState({
      collapsed: !this.state.collapsed,
      openDrawer: !this.state.openDrawer
    })

  onMouseHover = collapsed => this.setState({ collapsed })

  render() {
    const { collapsed, openDrawer } = this.state

    return (
      <DashAppHolder>
        <AppHolder>
          {/* Header */}
          <Topbar collapsed={collapsed} onClick={this.onDrawerClick} />

          <Layout style={{ flexDirection: 'row', overflowX: 'hidden' }}>
            <Sidebar
              collapsed={collapsed}
              openDrawer={openDrawer}
              options={this.props.routes}
              onMouseHover={this.onMouseHover}
            />

            <Layout className="isoContentMainLayout">
              {/* Body */}
              <Content className="isomor bodyContent">
                <PageHeader>{this.props.title}</PageHeader>
                <LayoutContentWrapper
                  style={{
                    flexShrink: '0',
                    position: 'relative'
                  }}
                >
                  {this.props.children}
                </LayoutContentWrapper>
              </Content>
              <div
                style={{
                  textAlign: 'center',
                  paddingTop: '40px',
                  color: '#bebebe'
                }}
              >
                &copy; Copyright 2019 by cloudbox
              </div>
            </Layout>
          </Layout>
        </AppHolder>
      </DashAppHolder>
    )
  }
}
