import React from 'react'
import { Link } from 'react-router-dom'
import Popover from 'components/uielements/popover'

import ButtonCellView from './buttonCell'
import ImageCellView from './imageCell'
import DeleteCellView from './deleteCell'
import FilterDropdown from './filterDropdown'

const ButtonCell = ({
  title = 'Title',
  okText = 'Ok',
  cancelText = 'cancel',
  buttonLabel = 'Button',
  onButtonCell = () => console.log('Button cell clicked!'),
  onCancel = () => console.log('Button cell canceled'),
  buttonLink = false,
  hasConfirm = true
}) => {
  return (
    <ButtonCellView
      title={title}
      okText={okText}
      cancelText={cancelText}
      buttonLabel={buttonLabel}
      hasConfirm={hasConfirm}
      buttonLink={buttonLink}
      onCancel={onCancel}
      onButtonCell={onButtonCell}
    />
  )
}

const ButtonGroupCell = buttons => {
  return (
    <span style={{ display: 'flex', flexDirection: 'column' }}>
      {buttons.map(button =>
        <p>
          {button}
        </p>
      )}
    </span>
  )
}

const DeleteCell = text => {
  return <DeleteCellView />
}

const DateCell = data => {
  return (
    <p>
      {data.toLocaleString()}
    </p>
  )
}

const ImageCell = src => {
  return <ImageCellView src={src} />
}

const LinkCell = (link, href) => {
  return (
    <Link to={href}>
      {link}
    </Link>
  )
}

const TextCell = text => {
  return (
    <span>
      {text}
    </span>
  )
}

const DesCell = text => {
  return (
    <Popover title={null} content={text}>
      <p
        style={{
          maxWidth: '100px',
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          display: 'inline-block',
          textOverflow: 'ellipsis'
        }}
      >
        {text}
      </p>
    </Popover>
  )
}

export {
  ButtonCell,
  ButtonGroupCell,
  DateCell,
  ImageCell,
  LinkCell,
  TextCell,
  DesCell,
  DeleteCell,
  FilterDropdown
}
