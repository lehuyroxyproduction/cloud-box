import configFactory from 'config'

import React from 'react'
import Proptypes from 'prop-types'
import { Link } from 'react-router-dom'

import Button from 'components/uielements/button'

const config = configFactory()

import { TableDemoStyle, TableWrapper } from './styles'
import {
  DateCell,
  DeleteCell,
  ImageCell,
  LinkCell,
  TextCell,
  DesCell
} from './cells'

export default class Table extends React.Component {
  pagination = config.pagination_num

  state = { columns: null }

  static propTypes = {
    loading: Proptypes.bool,
    columns: Proptypes.array,
    dataSource: Proptypes.array,
    searchFields: Proptypes.array,
    topRightButton: Proptypes.shape({
      title: Proptypes.string,
      link: Proptypes.string
    })
  }

  static defaultProps = {
    loading: false,
    columns: [],
    dataSource: []
  }

  componentDidMount() {
    this.createColumns()
  }

  createColumns() {
    this.props.columns &&
      this.setState({
        columns: this.props.columns.map(
          ({ title, key, width, cell, link, callback_params, render }) => {
            return {
              title,
              key,
              width,
              render: object => {
                if (render) {
                  if (callback_params) {
                    let params = []

                    callback_params.map(param => {
                      if (object.hasOwnProperty(param)) {
                        params.push(object[param])
                      }
                    })
                    return render(params)
                  } else {
                    return render()
                  }
                }

                const linkParam = object.linkParam

                return this.renderCell(object, cell, key, link, linkParam)
              }
            }
          }
        )
      })
  }

  renderCell = (object, type, key, link, linkParam) => {
    const value = object[key]

    switch (type) {
      case 'ImageCell':
        return ImageCell(value)
      case 'DateCell':
        return DateCell(value)
      case 'LinkCell':
        return LinkCell(value, link + linkParam)
      case 'DesCell':
        return DesCell(value)
      case 'DeleteCell':
        return DeleteCell(value)
      default:
        return TextCell(value)
    }
  }

  renderTopRightButton() {
    const { url, title, onClick = () => {} } = this.props.topRightButton

    const buttonWrapper = {
      marginBottom: 24,
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end'
    }

    return (
      <div style={buttonWrapper} onClick={onClick}>
        {url.includes('http')
          ? <a href={url}>
              <Button type="primary">
                {title}
              </Button>
            </a>
          : <Link to={`${url}`}>
              <Button type="primary">
                {title}
              </Button>
            </Link>}
      </div>
    )
  }

  onPageChange = pageNumber => {
    this.props.onPageChange(pageNumber)
  }

  render() {
    const { columns } = this.state
    const { total, loading, dataSource, topRightButton } = this.props

    return (
      <TableDemoStyle className="isoLayoutContent">
        {topRightButton && this.renderTopRightButton()}

        <TableWrapper
          loading={loading}
          pagination={{
            defaultCurrent: 1,
            total,
            defaultPageSize: 5,
            onChange: this.onPageChange
          }}
          columns={columns}
          dataSource={dataSource}
          className="isoSimpleTable"
          title={this.props.renderHeader && this.props.renderHeader}
        />
      </TableDemoStyle>
    )
  }
}
