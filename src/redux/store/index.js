import { createStore, applyMiddleware } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import rootReducer from '../modules'
import { composeWithDevTools } from 'redux-devtools-extension'

export const history = createHistory()
const defaultState = {}
const middleware = [thunk, routerMiddleware(history)]

const store = createStore(
  rootReducer,
  defaultState,
  // http://extension.remotedev.io/
  composeWithDevTools(applyMiddleware(...middleware))
)

export default store
