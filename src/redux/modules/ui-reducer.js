import { combineReducers } from 'redux'
import { ReducerFactory } from '@/ReducerFactory'

// login
import { types as typesLogin } from './auth-reducer'
import { types as typesAccount } from './account-reducer'
import { types as typesUser } from './user-reducer'
import { types as typesCustomer } from './customer-reducer'
import { types as typesStaff } from './staff-reducer'
import { types as typesQuestion } from './question-reducer'
import { types as typesCampaign } from './campaign-reducer'
import { types as typesFAQ } from './faq-reducer'

const defaultState = {
  isInit: true,
  isLoading: false,
  isSuccess: false,
  isError: false,
  reason: ''
}
//AUTH
const auth = combineReducers({
  login: ReducerFactory({
    defaultState,
    onStart: typesLogin.LOGIN,
    onSuccess: typesLogin.LOGIN_SUCCESS,
    onError: typesLogin.LOGIN_FAIL,
    onDefault: typesLogin.LOGOUT
  })
})
//ACCOUNT
const account = combineReducers({
  getAccounts: ReducerFactory({
    defaultState,
    onStart: typesAccount.GET_ACCOUNTS,
    onSuccess: typesAccount.GET_ACCOUNTS_SUCCESS,
    onError: typesAccount.GET_ACCOUNTS_FAIL,
    onDefault: typesLogin.LOGIN
  })
})
//user
const user = combineReducers({
  getUsers: ReducerFactory({
    defaultState,
    onStart: typesUser.GET_USERS,
    onSuccess: typesUser.GET_USERS_SUCCESS,
    onError: typesUser.GET_USERS_FAIL,
    onDefault: typesLogin.LOGIN
  })
})
//customer
const customer = combineReducers({
  getCustomers: ReducerFactory({
    defaultState,
    onStart: typesCustomer.GET_CUSTOMERS,
    onSuccess: typesCustomer.GET_CUSTOMERS_SUCCESS,
    onError: typesCustomer.GET_CUSTOMERS_FAIL,
    onDefault: typesLogin.LOGIN
  })
})
//staff
const staff = combineReducers({
  getStaffs: ReducerFactory({
    defaultState,
    onStart: typesStaff.GET_STAFFS,
    onSuccess: typesStaff.GET_STAFFS_SUCCESS,
    onError: typesStaff.GET_STAFFS_FAIL,
    onDefault: typesLogin.LOGIN
  })
})

//question
const question = combineReducers({
  getQuestions: ReducerFactory({
    defaultState,
    onStart: typesQuestion.GET_QUESTIONS,
    onSuccess: typesQuestion.GET_QUESTIONS_SUCCESS,
    onError: typesQuestion.GET_QUESTIONS_FAIL,
    onDefault: typesLogin.LOGIN
  }),
  getMyQuestions: ReducerFactory({
    defaultState,
    onStart: typesQuestion.GET_MYQUESTIONS,
    onSuccess: typesQuestion.GET_MYQUESTIONS_SUCCESS,
    onError: typesQuestion.GET_MYQUESTIONS_FAIL,
    onDefault: typesLogin.LOGIN
  })
})

//campaign
const campaign = combineReducers({
  getCampaigns: ReducerFactory({
    defaultState,
    onStart: typesCampaign.GET_CAMPAIGNS,
    onSuccess: typesCampaign.GET_CAMPAIGNS_SUCCESS,
    onError: typesCampaign.GET_CAMPAIGNS_FAIL,
    onDefault: typesLogin.LOGIN
  }),
  checkCode: ReducerFactory({
    defaultState,
    onStart: typesCampaign.CHECK_CODE,
    onSuccess: typesCampaign.CHECK_CODE_SUCCESS,
    onError: typesCampaign.CHECK_CODE_FAIL,
    onDefault: typesLogin.LOGIN
  }),
  redeemCode: ReducerFactory({
    defaultState,
    onStart: typesCampaign.REDEEM_CODE,
    onSuccess: typesCampaign.REDEEM_CODE_SUCCESS,
    onError: typesCampaign.REDEEM_CODE_FAIL,
    onDefault: typesLogin.LOGIN
  })
})

//faq
const faq = combineReducers({
  getFaqs: ReducerFactory({
    defaultState,
    onStart: typesFAQ.GET_FAQS,
    onSuccess: typesFAQ.GET_FAQS_SUCCESS,
    onError: typesFAQ.GET_FAQS_FAIL,
    onDefault: typesLogin.LOGIN
  })
})

export default combineReducers({
  auth,
  account,
  user,
  customer,
  staff,
  question,
  campaign,
  faq
})
