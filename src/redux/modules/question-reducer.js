import { api } from 'services'
import configFactory from 'config'
const config = configFactory()

const PAGE_SIZE = config.page_size

// Constants
export const types = {
  // Get questions
  GET_QUESTIONS: 'GET_QUESTIONS',
  GET_QUESTIONS_SUCCESS: 'GET_QUESTIONS_SUCCESS',
  GET_QUESTIONS_FAIL: 'GET_QUESTIONS_FAIL',
  // Get my questions
  GET_MYQUESTIONS: 'GET_MYQUESTIONS',
  GET_MYQUESTIONS_SUCCESS: 'GET_MYQUESTIONS_SUCCESS',
  GET_MYQUESTIONS_FAIL: 'GET_MYQUESTIONS_FAIL',
  // Add user
  ADD_QUESTION: 'ADD_QUESTION',
  ADD_QUESTION_SUCCESS: 'ADD_QUESTION_SUCCESS',
  ADD_QUESTION_FAIL: 'ADD_QUESTION_FAIL',
  // Update QUESTION
  UPDATE_QUESTION: 'UPDATE_QUESTION',
  UPDATE_QUESTION_SUCCESS: 'UPDATE_QUESTION_SUCCESS',
  UPDATE_QUESTION_FAIL: 'UPDATE_QUESTION_FAIL',
  //Delete QUESTIONs
  DELETE_QUESTION: 'DELETE_QUESTION',
  DELETE_QUESTION_SUCCESS: 'DELETE_QUESTION_SUCCESS',
  DELETE_QUESTION_FAIL: 'DELETE_QUESTION_FAIL',
  // Get QUESTION info
  GET_QUESTION_INFO: 'GET_QUESTION_INFO',
  GET_QUESTION_INFO_SUCCESS: 'GET_QUESTION_INFO_SUCCESS',
  GET_QUESTION_INFO_FAIL: 'GET_QUESTION_INFO_FAIL'
}

// Get question
const getQuestions = ({ name, status, page, limit }) => {
  return dispatch => {
    dispatch({ type: types.GET_QUESTIONS })
    api
      .getQuestions({ name, status, page, limit })
      .then(res => {
        console.log(res.data.data.helpdesks)
        if (res && res.data) {
          console.log('all')
          console.log(res)
          dispatch({
            type: types.GET_QUESTIONS_SUCCESS,
            payload: {
              list: res.data.data.helpdesks,
              page: res.data.data.paging
            }
          })
        } else {
          dispatch({ type: types.GET_QUESTIONS_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_QUESTIONS_FAIL }))
  }
}

const getMyQuestions = ({ page, limit }) => {
  return dispatch => {
    dispatch({ type: types.GET_MYQUESTIONS })
    api
      .getMyQuestions({ page, limit })
      .then(res => {
        console.log(res.data.data.helpdesks)
        if (res && res.data) {
          console.log('my')
          dispatch({
            type: types.GET_MYQUESTIONS_SUCCESS,
            payload: {
              list: res.data.data.helpdesks,
              page: res.data.data.paging
            }
          })
        } else {
          dispatch({ type: types.GET_MYQUESTIONS_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_MYQUESTIONS_FAIL }))
  }
}

// Update question
const updatedQuestion = params => {
  console.log(params)

  return dispatch => {
    dispatch({ type: types.UPDATE_QUESTION })
    api
      .updatedQuestion(params)
      .then(res => {
        if (res && res.data) {
          console.log(res)
          dispatch({
            type: types.UPDATE_QUESTION_SUCCESS
          })
        } else {
          dispatch({ type: types.UPDATE_QUESTION_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.UPDATE_QUESTION_FAIL }))
  }
}

// Actions
export const actions = {
  getQuestions,
  getMyQuestions,
  updatedQuestion
}

// Selectors
export const selectors = {
  getPage: state => state.question.page,
  getTotal: state => state.question.page.pageTotal,
  getQuestionsList: state => state.question.list || [],
  getPageTotal: state => state.question.page || null,
  getQuestionInfo: state => state.question.info || {},
  getLoading: (state, action) => state.ui.question[action].isLoading
}

const defaultState = {
  date: {},
  list: [],
  page: null,
  info: {}
  // days: {}
}

const questionReducer = (state = defaultState, action) => {
  const nextState = Object.assign({}, state)
  switch (action.type) {
    case types.GET_QUESTIONS_SUCCESS:
      return {
        ...nextState,
        list: action.payload.list,
        page: action.payload.page
      }
    case types.GET_MYQUESTIONS_SUCCESS:
      return {
        ...nextState,
        list: action.payload.list,
        page: action.payload.page
      }
    // case types.ADD_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_USER_INFO_SUCCESS:
    //   return {
    //     ...nextState,
    //     info: action.payload
    //   }
    // case types.EDIT_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_AVAILABLE_USERS_SUCCESS:
    //   return {
    //     ...nextState,
    //     listAvai: action.payload.listAvai,
    //     page: action.payload.page
    //   }
    default:
      return state
  }
}

export default questionReducer
