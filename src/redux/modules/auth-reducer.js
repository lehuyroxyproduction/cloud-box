import jwt from 'jsonwebtoken'
import axios from 'axios'

import { api } from 'services'

import Notification from 'components/notification'

// Constants
export const types = {
  // Set user
  SET_USER: 'SET_USER',
  // Login
  LOGIN: 'LOGIN',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_FAIL: 'LOGIN_FAIL',
  // Log out
  LOGOUT: 'LOGOUT',
  LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',
  LOGOUT_FAIL: 'LOGOUT_FAIL',
  // Register
  REGISTER: 'REGISTER',
  REGISTER_SUCCESS: 'REGISTER_SUCCESS',
  REGISTER_FAIL: 'REGISTER_FAIL',
  // Get user info
  GET_USER_INFO: 'GET_USER_INFO',
  GET_USER_INFO_SUCCESS: 'GET_USER_INFO_SUCCESS',
  GET_USER_INFO_FAIL: 'GET_USER_INFO_FAIL'
}
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// const loginApp = async (email, password) => {
//   try {
//     //xu li await api
//     const response = await axios.post('http://18.212.20.61:8000/api/login', JSON.stringify({ email, password }))
//     return response.data
//   } catch (err) {
//     throw err
//   }
// }

//login -hoan
const login = user => {
  return dispatch => {
    dispatch({ type: types.LOGIN })
    api
      .login(user)
      .then(response => {
        console.log(response)

        if (response.data.status_code !== 1000) {
          dispatch({ type: types.LOGIN_FAIL })
          Notification('error', 'ERROR', response.data.message)
          return
        }
        const { name, role, services } = response.data.data
        localStorage.setItem('user', JSON.stringify({ name, role }))
        localStorage.setItem('menu', JSON.stringify(services))
        const token = response.data.data.token
        localStorage.setItem('token', token)
        dispatch({
          type: types.LOGIN_SUCCESS,
          payload: response.data
        })
      })
      .catch(err => {
        dispatch({ type: types.LOGIN_FAIL })
        Notification('error', 'ERROR', err.message)
      })
  }
}
// Set user
const setUser = user => ({ type: types.SET_USER, payload: user })

// // Log in
// const login = ({ userID, password }) => {
//   return dispatch => {
//     dispatch({ type: types.LOGIN })

//     api
//       .login({ userID, password })
//       .then(res => {
//         if (res && res.data.retData) {
//           const token = res.data.retData.token
//           const user = jwt.decode(token)
//           localStorage.setItem("token", token)
//           localStorage.setItem("name", res.data.retData.name)
//           dispatch({ type: types.LOGIN_SUCCESS, payload: user })
//         } else {
//           Notification("error", "THẤT BẠI!", res.data.retMessage)
//           dispatch({ type: types.LOGIN_FAIL })
//         }
//       })
//       .catch(err => dispatch({ type: types.LOGIN_FAIL }))
//   }
// }
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// const login = ({ username, password }) => {
//   return dispatch => {
//     dispatch({ type: types.LOGIN })

//     loginApp(username, password)
//       .then(response => {
//         if (response.status_code !== 1000) {
//           dispatch({ type: types.LOGIN_FAIL })
//           Notification('error', 'ERROR', response.message)
//           return
//         }
//         localStorage.setItem('user', response.data)
//         dispatch({
//           type: types.LOGIN_SUCCESS,
//           payload: response.data
//         })
//       })
//       .catch(err => {
//         dispatch({ type: types.LOGIN_FAIL })
//         Notification('error', 'ERROR', err.message)
//       })
//     //----------------------------
//     // if (username == 'admin' && password == '12345') {
//     //   localStorage.setItem('token', 'A1B2C3')
//     //   localStorage.setItem('name', 'Admin')

//     //   dispatch({
//     //     type: types.LOGIN_SUCCESS,
//     //     payload: { username: 123, username: 'Admin' }
//     //   })
//     // } else {
//     //   dispatch({ type: types.LOGIN_FAIL })
//     //   Notification('error', 'FAILURE', 'invalid userID or password!')
//     // }
//     //-------------------------------
//   }
// }

// Log out
const logout = () => {
  return dispatch => {
    dispatch({ type: types.LOGOUT })
    localStorage.removeItem('token')
  }
}

// Register
const register = info => {
  const {
    role,
    avatar,
    first_name,
    last_name,
    password,
    phone_number,
    userID
  } = info

  return dispatch => {
    dispatch({ type: types.REGISTER })

    // api
    //   .register({
    //     role,
    //     avatar,
    //     first_name,
    //     last_name,
    //     password,
    //     phone_number,
    //     userID
    //   })
    //   .then(res => {
    //     if (res && res.status_code === 1000) {
    //       dispatch({ type: types.REGISTER_SUCCESS })

    //       Notification('success', 'THÀNH CÔNG!')

    //       setTimeout(
    //         () => (window.location = window.location.href.split('register')[0]),
    //         1000
    //       )
    //     } else {
    //       dispatch({ type: types.REGISTER_FAIL })

    //       Notification('error', 'THẤT BẠI!', res.message)
    //     }
    //   })
    //   .catch(err => {
    //     dispatch({ type: types.REGISTER_FAIL })
    //   })
  }
}
// action get user info
const getUserInfo = id => {
  return dispatch => {
    dispatch({ type: types.GET_USER_INFO })
    api.getAccountInfo(id).then(res => {
      if (res && res.data.retData) {
        dispatch({
          type: types.GET_USER_INFO_SUCCESS,
          payload: res.data.retData
        })
      } else {
        dispatch({ type: types.GET_USER_INFO_FAIL })
      }
    })
  }
}

// Actions
export const actions = {
  setUser,
  login,
  getUserInfo,
  logout,
  register
}

// Selectors
export const selectors = {
  getUser: state => state.auth.user,
  getUserRole: state => state.auth.user.roleID,
  getLoading: (state, actions) => state.ui.auth[actions].isLoading,
  getErrorStatus: (state, actions) => state.ui.auth[actions].isError,
  getSuccessStatus: (state, actions) => state.ui.auth[actions].isSuccess
}

// Default state
const defaultState = { user: null }

// Reducer
const authReducer = (state = defaultState, action) => {
  const nextState = Object.assign({}, state)

  switch (action.type) {
    case types.SET_USER:
      return { ...state, user: action.payload }
    case types.LOGIN_SUCCESS:
      return { ...state, user: { userID: action.payload.userID } }
    case types.GET_USER_INFO_SUCCESS:
      return { ...state, user: action.payload }
    case types.LOGOUT:
      return defaultState
    default:
      return nextState
  }
}

export default authReducer
