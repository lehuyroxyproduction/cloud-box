import { api } from 'services'
import configFactory from 'config'
const config = configFactory()

const PAGE_SIZE = config.page_size

// Constants
export const types = {
  // Get users
  GET_USERS: 'GET_USERS',
  GET_USERS_SUCCESS: 'GET_USERS_SUCCESS',
  GET_USERS_FAIL: 'GET_USERS_FAIL',
  // Get users_STATISTICS
  GET_USERS_STATISTICS: 'GET_USERS_STATISTICS',
  GET_USERS_STATISTICS_SUCCESS: 'GET_USERS_STATISTICS_SUCCESS',
  GET_USERS_STATISTICS_FAIL: 'GET_USERS_STATISTICS_FAIL',
  // Add user
  ADD_USER: 'ADD_USER',
  ADD_USER_SUCCESS: 'ADD_USER_SUCCESS',
  ADD_USER_FAIL: 'ADD_USER_FAIL',
  // Edit USER
  EDIT_USER: 'EDIT_USER',
  EDIT_USER_SUCCESS: 'EDIT_USER_SUCCESS',
  EDIT_USER_FAIL: 'EDIT_USER_FAIL',
  //Delete USERs
  DELETE_USER: 'DELETE_USER',
  DELETE_USER_SUCCESS: 'DELETE_USER_SUCCESS',
  DELETE_USER_FAIL: 'DELETE_USER_FAIL',
  // Get USER info
  GET_USER_INFO: 'GET_USER_INFO',
  GET_USER_INFO_SUCCESS: 'GET_USER_INFO_SUCCESS',
  GET_USER_INFO_FAIL: 'GET_USER_INFO_FAIL'
}

// Get users
const getUsers = ({ name, page, limit }) => {
  console.log(limit)

  return dispatch => {
    dispatch({ type: types.GET_USERS })
    api
      .getUsers({ name, page, limit })
      .then(res => {
        if (res && res.data) {
          dispatch({
            type: types.GET_USERS_SUCCESS,
            payload: { list: res.data.data.users, page: res.data.data.paging }
          })
        } else {
          dispatch({ type: types.GET_USERS_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_USERS_FAIL }))
  }
}

// Actions
export const actions = {
  getUsers
}

// Selectors
export const selectors = {
  getPage: state => state.user.page,
  getTotal: state => state.user.page.pageTotal,
  getUsersList: state => state.user.list || [],
  getStatistics: state => state.user.date || {},
  getPageTotal: state => state.user.page || null,
  getUserInfo: state => state.user.info || {},
  getLoading: (state, action) => state.ui.user[action].isLoading
  // exportUsers: state => state.user.days || {},
}

const defaultState = {
  date: {},
  list: [],
  page: null,
  info: {}
  // days: {}
}

const userReducer = (state = defaultState, action) => {
  const nextState = Object.assign({}, state)
  switch (action.type) {
    case types.GET_USERS_SUCCESS:
      return {
        ...nextState,
        list: action.payload.list,
        page: action.payload.page
      }
    // case types.ADD_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_USER_INFO_SUCCESS:
    //   return {
    //     ...nextState,
    //     info: action.payload
    //   }
    // case types.EDIT_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_AVAILABLE_USERS_SUCCESS:
    //   return {
    //     ...nextState,
    //     listAvai: action.payload.listAvai,
    //     page: action.payload.page
    //   }
    default:
      return state
  }
}

export default userReducer
