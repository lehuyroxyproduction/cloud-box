import { api } from 'services'

import Notification from 'components/notification'

import configFactory from 'config'
const config = configFactory()

const PAGE_SIZE = config.page_size

// Constants
export const types = {
  // Get accounts
  GET_ACCOUNTS: 'GET_ACCOUNTS',
  GET_ACCOUNTS_SUCCESS: 'GET_ACCOUNTS_SUCCESS',
  GET_ACCOUNTS_FAIL: 'GET_ACCOUNTS_FAIL',
  // Add account
  ADD_ACCOUNT: 'ADD_ACCOUNT',
  ADD_ACCOUNT_SUCCESS: 'ADD_ACCOUNT_SUCCESS',
  ADD_ACCOUNT_FAIL: 'ADD_ACCOUNT_FAIL',
  // Edit account
  EDIT_ACCOUNT: 'EDIT_ACCOUNT',
  EDIT_ACCOUNT_SUCCESS: 'EDIT_ACCOUNT_SUCCESS',
  EDIT_ACCOUNT_FAIL: 'EDIT_ACCOUNT_FAIL',
  // // Reset account
  // RESET_ACCOUNT: "UPDATE_SUPPLIER_INFO",
  // RESET_ACCOUNT_SUCCESS: "UPDATE_SUPPLIER_INFO_SUCCESS",
  // RESET_ACCOUNT_FAIL: "UPDATE_SUPPLIER_INFO_FAIL",
  // Get account info
  GET_ACCOUNT_INFO: 'GET_ACCOUNT_INFO',
  GET_ACCOUNT_INFO_SUCCESS: 'GET_ACCOUNT_INFO_SUCCESS',
  GET_ACCOUNT_INFO_FAIL: 'GET_ACCOUNT_INFO_FAIL',
  //Delete account
  DELETE_ACCOUNT: 'DELETE_ACCOUNT',
  DELETE_ACCOUNT_SUCCESS: 'DELETE_ACCOUNT_SUCCESS',
  DELETE_ACCOUNT_FAIL: 'DELETE_ACCOUNT_FAIL',
  //Delete account
  PASSWORD_CHANGE: 'PASSWORD_CHANGE',
  PASSWORD_CHANGE_SUCCESS: 'PASSWORD_CHANGE_SUCCESS',
  PASSWORD_CHANGE_FAIL: 'PASSWORD_CHANGE_FAIL'
}

// Get accounts
const getAccounts = ({ name, page_index, page_size }) => {
  return dispatch => {
    dispatch({ type: types.GET_ACCOUNTS })

    api
      .getAccounts({ name, page_index, page_size })
      .then(res => {
        if (res && res.data.retData) {
          dispatch({
            type: types.GET_ACCOUNTS_SUCCESS,
            payload: { list: res.data.retData, page: res.data.retPaging }
          })
        } else {
          dispatch({ type: types.GET_ACCOUNTS_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_ACCOUNTS_FAIL }))
  }
}

// Add account
const addAccount = ({ userID, roleID }) => {
  return dispatch => {
    dispatch({ type: types.ADD_ACCOUNT })

    api
      .addAccount({ userID, roleID })
      .then(res => {
        if (res && res.data) {
          dispatch({
            type: types.ADD_ACCOUNT_SUCCESS,
            payload: res.data
          })
          dispatch(
            actions.getAccounts({
              name: '',
              page_index: 1,
              page_size: PAGE_SIZE
            })
          )
        } else {
          dispatch({ type: types.ADD_ACCOUNT_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.ADD_ACCOUNT_FAIL }))
  }
}
// Get account info
const getAccountInfo = userID => {
  return dispatch => {
    dispatch({ type: types.GET_ACCOUNT_INFO })
    api
      .getAccountInfo(userID)
      .then(res => {
        if (res && res.data.retData) {
          dispatch({
            type: types.GET_ACCOUNT_INFO_SUCCESS,
            payload: res.data.retData
          })
        } else {
          dispatch({ type: types.GET_ACCOUNT_INFO_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_ACCOUNT_INFO_FAIL }))
  }
}
// Delete account
const deleteAccount = userID => {
  return dispatch => {
    dispatch({ type: types.DELETE_ACCOUNT })
    api.deleteAccount(userID).then(res => {
      if (res && res.data.retMessage) {
        dispatch({ type: types.DELETE_ACCOUNT_SUCCESS })
        if (res.data.retCode !== 1002) {
          dispatch(
            actions.getAccounts({
              name: '',
              page_index: 1,
              page_size: PAGE_SIZE
            })
          )
        }
      } else {
        dispatch({ type: types.DELETE_ACCOUNT_FAIL })
      }
    })
  }
}

//Edit account
const editAccount = (obj, id) => {
  return dispatch => {
    dispatch({ type: types.EDIT_ACCOUNT })
    api
      .editAccount(obj, id)
      .then(res => {
        if (res && res.data.retMessage) {
          dispatch({ type: EDIT_ACCOUNT_SUCCESS, payload: res.data })
          dispatch(
            actions.getAccounts({
              name: '',
              page_index: 1,
              page_size: PAGE_SIZE
            })
          )
        } else {
          dispatch({ type: types.EDIT_ACCOUNT_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.EDIT_ACCOUNT_FAIL }))
  }
}

//Edit account
const passwordChange = obj => {
  console.log(obj)

  return dispatch => {
    dispatch({ type: types.PASSWORD_CHANGE })
    api
      .ChangePassword(obj)
      .then(res => {
        console.log(res)

        if (res && res.data.retMessage) {
          dispatch({ type: PASSWORD_CHANGE_SUCCESS, payload: res.data })
        } else {
          dispatch({ type: types.PASSWORD_CHANGE_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.PASSWORD_CHANGE_FAIL }))
  }
}

// Actions
export const actions = {
  getAccounts,
  addAccount,
  getAccountInfo,
  deleteAccount,
  editAccount,
  passwordChange
}

// Selectors
export const selectors = {
  getPage: state => state.account.page,
  getTotal: state => state.account.page.pageTotal,
  getAccountsList: state => state.account.list || [],
  getPageTotal: state => state.account.page || null,
  getAccountInfo: state => state.account.info || {},
  getLoading: (state, action) => state.ui.account[action].isLoading
}

const defaultState = {
  list: [],
  page: null,
  info: {}
}

const accountReducer = (state = defaultState, action) => {
  const nextState = Object.assign({}, state)
  switch (action.type) {
    case types.GET_ACCOUNTS_SUCCESS:
      return {
        ...state,
        list: action.payload.list,
        page: action.payload.page
      }
    case types.ADD_ACCOUNT_SUCCESS:
      return { ...nextState }
    case types.GET_ACCOUNT_INFO_SUCCESS:
      return { ...nextState, info: action.payload }
    case types.EDIT_ACCOUNT_SUCCESS:
      return {
        ...nextState
      }
    default:
      return state
  }
}

export default accountReducer
