import { api } from 'services'
import configFactory from 'config'
const config = configFactory()
import Notification from 'components/notification'

const PAGE_SIZE = config.page_size

// Constants
export const types = {
  // Get faqs
  GET_FAQS: 'GET_FAQS',
  GET_FAQS_SUCCESS: 'GET_FAQS_SUCCESS',
  GET_FAQS_FAIL: 'GET_FAQS_FAIL',
  // Get my faqs
  GET_MYFAQS: 'GET_MYFAQS',
  GET_MYFAQS_SUCCESS: 'GET_MYFAQS_SUCCESS',
  GET_MYFAQS_FAIL: 'GET_MYFAQS_FAIL',
  // Add user
  ADD_FAQ: 'ADD_FAQ',
  ADD_FAQ_SUCCESS: 'ADD_FAQ_SUCCESS',
  ADD_FAQ_FAIL: 'ADD_FAQ_FAIL',
  // Update FAQ
  UPDATE_FAQ: 'UPDATE_FAQ',
  UPDATE_FAQ_SUCCESS: 'UPDATE_FAQ_SUCCESS',
  UPDATE_FAQ_FAIL: 'UPDATE_FAQ_FAIL',
  //Delete FAQs
  DELETE_FAQ: 'DELETE_FAQ',
  DELETE_FAQ_SUCCESS: 'DELETE_FAQ_SUCCESS',
  DELETE_FAQ_FAIL: 'DELETE_FAQ_FAIL',
  // Get FAQ info
  GET_FAQ_INFO: 'GET_FAQ_INFO',
  GET_FAQ_INFO_SUCCESS: 'GET_FAQ_INFO_SUCCESS',
  GET_FAQ_INFO_FAIL: 'GET_FAQ_INFO_FAIL',
  // Get FAQ info
  CHECK_CODE: 'CHECK_CODE',
  CHECK_CODE_SUCCESS: 'CHECK_CODE_SUCCESS',
  CHECK_CODE_FAIL: 'CHECK_CODE_FAIL',
  // Get FAQ info
  REDEEM_CODE: 'REDEEM_CODE',
  REDEEM_CODE_SUCCESS: 'REDEEM_CODE_SUCCESS',
  REDEEM_CODE_FAIL: 'REDEEM_CODE_FAIL'
}

// Get faq
const getFaqs = () => {
  return dispatch => {
    dispatch({ type: types.GET_FAQS })
    api
      .getFaqs()
      .then(res => {
        if (res && res.data) {
          console.log(res)
          dispatch({
            type: types.GET_FAQS_SUCCESS,
            payload: {
              list: res.data.data
            }
          })
        } else {
          dispatch({ type: types.GET_FAQS_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_FAQS_FAIL }))
  }
}
// Get faq
const addFaq = obj => {
  console.log(obj)

  return dispatch => {
    dispatch({ type: types.ADD_FAQ })
    api
      .addFaq(obj)
      .then(res => {
        if (res && res.data) {
          dispatch({
            type: types.ADD_FAQ_SUCCESS
          })
          Notification('success', 'SUCCESS', res.data.message)
        } else {
          dispatch({ type: types.ADD_FAQ_FAIL })
          Notification('error', 'error', res.data.message)
        }
      })
      .catch(err => dispatch({ type: types.ADD_FAQ_FAIL }))
  }
}

// Get faq
const getFaqInfo = id => {
  return dispatch => {
    dispatch({ type: types.GET_FAQ_INFO })
    api
      .getFaqInfo(id)
      .then(res => {
        if (res && res.data) {
          dispatch({
            type: types.GET_FAQ_INFO_SUCCESS,
            payload: res.data.data
          })
        } else {
          dispatch({ type: types.GET_FAQ_INFO_FAIL })
        }
      })
      .catch(err => dispatch({ type: types.GET_FAQ_INFO_FAIL }))
  }
}

// Actions
export const actions = {
  getFaqs,
  addFaq,
  getFaqInfo
}

// Selectors
export const selectors = {
  getPage: state => state.faq.page,
  getTotal: state => state.faq.page.pageTotal,
  getFaqsList: state => state.faq.list || [],
  getPageTotal: state => state.faq.page || null,
  getFaqInfo: state => state.faq.info || {},
  checkCode: state => state.faq.codeInfo || {},
  redeemCode: state => state.faq.codeInfo2 || {},
  getLoading: (state, action) => state.ui.faq[action].isLoading
}

const defaultState = {
  date: {},
  list: [],
  page: null,
  info: {},
  codeInfo: {},
  codeInfo2: {}

  // days: {}
}

const faqReducer = (state = defaultState, action) => {
  const nextState = Object.assign({}, state)
  switch (action.type) {
    case types.GET_FAQS_SUCCESS:
      return {
        ...nextState,
        list: action.payload.list,
        page: action.payload.page
      }
    case types.GET_FAQ_INFO_SUCCESS:
      return {
        ...nextState,
        info: action.payload
      }
    case types.CHECK_CODE_SUCCESS:
      return {
        ...nextState,
        codeInfo: action.payload
      }
    case types.REDEEM_CODE_SUCCESS:
      return {
        ...nextState,
        codeInfo2: action.payload
      }
    // case types.ADD_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_USER_INFO_SUCCESS:
    //   return {
    //     ...nextState,
    //     info: action.payload
    //   }
    // case types.EDIT_USER_SUCCESS:
    //   return {
    //     ...nextState
    //   }
    // case types.GET_AVAILABLE_USERS_SUCCESS:
    //   return {
    //     ...nextState,
    //     listAvai: action.payload.listAvai,
    //     page: action.payload.page
    //   }
    default:
      return state
  }
}

export default faqReducer
