import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import { Popconfirm, message, Icon } from 'antd'

import { Button } from 'react-bootstrap'

import { Pagination } from 'antd'

import { queryPage } from 'helper'

import { actions, selectors } from 'redux/modules/document-reducer'
let timeout,
  status = 0
class DocumentList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      listBefore: [],
      list: [],
      page: {},
      filter: {
        name: '',
        page_index: 1,
        page_size: 20
      }
    }
  }

  componentDidMount() {
    if (!this.getQueryPage()) {
      this.setQueryPage(1)
    }
    this.setState(
      { filter: { ...this.state.filter, page_index: this.getQueryPage() } },
      () => {
        this.getDocuments()
      }
    )
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.list !== nextProps.list) {
      nextProps.setTitle('Công văn chứng từ')
      this.setState({ list: nextProps.list, page: nextProps.page })
    }
  }
  getDocuments = () => {
    const param = this.state.filter
    this.props.getDocuments(param)
  }
  setQueryPage = page => {
    return queryPage(this.props.history).set(page, '/cong-van-chung-tu')
  }
  getQueryPage = () => {
    return queryPage(this.props.history).get()
  }
  onChange = page => {
    this.setState(
      { filter: { ...this.state.filter, page_index: page } },
      () => {
        this.setQueryPage(page)
        this.getDocuments()
      }
    )
  }
  handleSearch = e => {
    clearTimeout(timeout)
    let value = e.target.value
    timeout = setTimeout(() => {
      this.setState(
        { filter: { name: value, page_index: 1, page_size: 50 } },
        () => {
          if (this.state.filter.name === '') {
            this.setState(
              {
                filter: { name: value, page_index: 1, page_size: 20 }
              },
              () => {
                this.getDocuments()
              }
            )
          } else this.getDocuments()
        }
      )
    }, 1000)
  }

  confirm(item) {
    this.setState({ listBefore: this.props.list })
    this.props.deleteDocument(item.id)
    setTimeout(() => {
      if (this.state.list.length === this.state.listBefore.length) {
        message.error('Không thể xóa')
      } else message.success('Đã xóa')
    }, 2500)
  }

  handleImport = () => {
    if (status === 0) {
      document.getElementById('uploadForm').style.display = 'block'
      status = 1
    } else {
      document.getElementById('uploadForm').style.display = 'none'
      status = 0
    }
  }

  onFormSubmit = e => {
    e.preventDefault() // Stop form submit
    this.fileUpload(this.state.file).then(() => {
      setTimeout(() => {
        window.location.reload()
      }, 500)
    })
  }

  onChangeInput = e => {
    this.setState({ file: e.target.files[0] })
  }

  renderItem(item) {
    const { id, name, typeName } = item

    return (
      <React.Fragment>
        <td>{id}</td>
        <td>{name}</td>
        <td>{typeName}</td>
        <td>
          <Link to={`/cong-van-chung-tu/chinh-sua?id=${id}`}>Sửa</Link> |{' '}
          <a href="#">
            <Popconfirm
              title="Bạn có chắc muốn xóa mục này ?"
              onConfirm={this.confirm.bind(this, item)}
              okText="Xóa"
              cancelText="Hủy"
            >
              Xóa
            </Popconfirm>
          </a>
        </td>
      </React.Fragment>
    )
  }

  render() {
    const { isLoading } = this.props
    const { page } = this.state

    return (
      <div style={{ width: '100%', height: '100%' }}>
        <div className="row pl-3">
          <input
            className="form-control col-md-6"
            placeholder="Tìm kiếm"
            onKeyUp={this.handleSearch}
          />
          <div className="col-md-6 text-right">
            <Button className="btn-theme ml-3" type="button">
              <Link to="/cong-van-chung-tu/them-moi">Thêm</Link>
            </Button>
          </div>
        </div>
        <div className="table-reponsive mt-5">
          {isLoading ? (
            <div className="w-100 text-center">
              <Icon type="loading" />
            </div>
          ) : (
            <table
              className="table table-bordered table-hover"
              id="tableTypeProduct"
              style={{ border: '1px solid #ddd' }}
            >
              <thead className="thead-light">
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Tên</th>
                  <th scope="col">Loại chứng từ</th>
                  <th scope="col">Công cụ</th>
                </tr>
              </thead>
              <tbody id="tableContent">
                {this.state.list.map(item => {
                  return (
                    <tr id={item.id} key={item.id}>
                      {this.renderItem(item)}
                    </tr>
                  )
                })}
              </tbody>
            </table>
          )}
        </div>
        <Pagination
          className="mt-5"
          current={page.pageIndex}
          onChange={this.onChange}
          total={page.itemTotal}
          pageSize={20}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  list: selectors.getDocumentsList(state),
  page: selectors.getPage(state),
  isLoading: selectors.getLoading(state, 'getDocuments')
})

const mapDispatchToProps = dispatch => ({
  getDocuments: obj => dispatch(actions.getDocuments(obj)),
  deleteDocument: id => dispatch(actions.deleteDocument(id))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DocumentList)
