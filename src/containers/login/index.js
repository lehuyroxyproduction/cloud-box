import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import logo from 'public/images/logo_big.png'
import { siteConfig } from 'settings'

import SignInStyleWrapper from './signin.style'

import Checkbox from 'components/uielements/checkbox'
import { FormGroup, FormControl } from 'react-bootstrap'
import { Button } from 'antd'
import bgLogin from 'public/images/img-login.png'

import { actions, selectors } from 'redux/modules/auth-reducer'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = { username: '', password: '' }
  }
  //xử lí sự kiện khi ấn vào bàn phím,sử dụng event.which thay cho event.button
  onKeyPress = e => {
    // press enter key
    if (e.which === 13) {
      this.loginUser(e)
      //nếu e.which == 13 xử lí event loginUser.
    }
  }

  loginUser = e => {
    //lưu lại giá trị nhập vào.
    e.preventDefault()
    //lấy giá trị người dùng nhập vào,this.state hiện tại đang được gán cho username ='',password =''
    const user = {
      email: this.state.username,
      password: this.state.password
    }
    this.props.loginUser(user)
  }

  handleChangeUsername = e => {
    this.setState({
      username: e.target.value
    })
  }

  handleChangePassword = e => {
    this.setState({
      password: e.target.value
    })
  }

  render() {
    const { user, isLoading } = this.props
    const { username, password } = this.state
    if (user) {
      // this.props.getUserInfo(user.username)
      return (
        <Redirect
          to={{
            // pathname: '/quan-ly-nhan-su'
            pathname: '/thong-tin'
          }}
        />
      )
    }

    return (
      <SignInStyleWrapper className="d-flex align-items-center justify-content-center">
        <div className="container-login container d-flex flex-column flex-md-row">
          <div className="row all-login-wrapper">
            <form className="col-12 col-md-4 mx-auto align-self-center form-login">
              <div className="text-title text-center mb-4">
                <img src={logo} height="80px" width="206px" />
              </div>
              <div className="form-group">
                <input
                  className="form-control-login"
                  autoFocus
                  type="text"
                  value={username}
                  onChange={this.handleChangeUsername}
                  placeholder="Email"
                  style={{ letterSpacing: '3px' }}
                />
              </div>
              <div className="form-group">
                <input
                  className="form-control-login"
                  value={password}
                  onChange={this.handleChangePassword}
                  onKeyPress={this.onKeyPress}
                  type="password"
                  placeholder="Mật khẩu"
                  style={{ letterSpacing: '5px', color: '#727272' }}
                />
              </div>
              <Button
                className="btn-login mt-4"
                loading={isLoading}
                onClick={this.loginUser}
              >
                Đăng nhập
              </Button>
              <div className="forgot-password text-center mt-3">
                <Link to="/quen-mat-khau">Quên mật khẩu?</Link>
              </div>
            </form>
            <div className="col-12 col-md-7 img-login">
              <img className="img-full" src={bgLogin} alt="Log In" />
            </div>
          </div>
        </div>
      </SignInStyleWrapper>
    )
  }
}

const mapStateToProps = state => ({
  user: selectors.getUser(state),
  isLoading: selectors.getLoading(state, 'login')
})

const mapDispatchToProps = dispatch => ({
  loginUser: account => dispatch(actions.login(account)),
  getUserInfo: userID => dispatch(actions.getUserInfo(userID))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)
