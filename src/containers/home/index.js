import React from 'react'

import { connect } from 'react-redux'

import HomeStyleWrapper from './index.style'

// const DEFAULT_USER_ID = defaultValue.userId
class HomePage extends React.Component {
  componentWillMount() {
    this.props.setTitle('Trang chủ')
  }
  render() {
    return(
      <HomeStyleWrapper style={{ width: '100%', height: '100%' }} >
        <h1>Ezbox</h1>
      </HomeStyleWrapper>
    ) 
  }
}

const mapStateToProps = () => ({
  // goals: selectors.getGoals(state)
})

const mapDispatchToProps = () => ({
  // getGoals: userId => dispatch(actions.getGoals(userId))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage)
