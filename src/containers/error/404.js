import React from 'react'

class ErrorPage404 extends React.Component {
  render() {
    return (
      <div className="text-center">
        <h3>404 - Not Found</h3>
      </div>
    )
  }
}

export default ErrorPage404
