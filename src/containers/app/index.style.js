import styled from 'styled-components'
import UTMHelve from 'public/fonts/UTMHelve.ttf'
import { Fonts } from '../../constants/constants'

export const MainStyle = styled.div`
  @font-face {
    font-family: 'UTMHelve';
    src: URL(${Fonts.normal}) format('truetype');
  }
  @font-face {
    font-family: 'UTMHelve-Bold';
    src: URL(${Fonts.bold}) format('truetype');
  }
  b {
    font-family: $ 'UTMHelve-Bold' !important;
  }

  html h1,
  html h2,
  html h3,
  html h4,
  html h5,
  html h6,
  html a,
  html p,
  html li,
  input,
  textarea,
  span,
  div,
  html,
  body,
  html a {
    margin-bottom: 0;
    font-family: 'UTMHelve';
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.004);
  }
`
