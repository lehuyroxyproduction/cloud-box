import styled from 'styled-components'
import { Colors } from 'constants/constants'

export const AnalysisStyle = styled.div`
    height:45px;
    min-width: 500px !important;
    max-width: 600px !important;
    background-color: ${Colors.grayBg}
    display:flex;
    line-height:45px;
    border-radius:8px;
    margin-left:23px;
    justify-content: left;
    > b {
        margin:0 10px
    }
`

export const ContainerStyle = styled.div`
  padding-bottom: 15px;
  .ant-modal-footer {
    width: 85%;
  }
`
