import React from 'react'
import store, { history } from '@/redux/store/index'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import App from '@/containers/app'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import 'react-block-ui/style.css'
import '@/styles/index.scss'

const appRender = Component => {
  render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Component />
      </ConnectedRouter>
    </Provider>,
    document.querySelector('#root')
  )
}

appRender(App)

// if (module.hot && module) {
//   module.hot.accept('@/containers/app', () => {
//     appRender(App)
//   })
// }
